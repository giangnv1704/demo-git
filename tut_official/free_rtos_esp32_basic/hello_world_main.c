/* Blink Example
   This example code is in the Public Domain (or CC0 licensed, at your option.)
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#include <string.h>
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"

#include "lwip/sys.h"

#define BLINK_GPIO 2

#define TASK1_BIT   (1UL << 0UL) // zero shift for bit0
#define TASK2_BIT   (1UL << 1UL) // 1 shift for flag  bit 1
#define TASK3_BIT   (1UL << 2UL) // 2 shift for flag bit 2

EventGroupHandle_t s_event_group;


#define xEX_QUEUES_ENABLE
#define USE_STRUCT_QUEUE        1


#define xUSE_SEMEPHORE

#define USE_MUTEX

          
void blink_task(void *pvParameter)
{
    /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
       muxed to GPIO on reset already, but some default to other
       functions and need to be switched to GPIO. Consult the
       Technical Reference for a list of pads and their default
       functions.)
    */
    gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    while(1) {
        /* Blink off (output low) */
        gpio_set_level(BLINK_GPIO, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        /* Blink on (output high) */
        gpio_set_level(BLINK_GPIO, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

#ifdef EX_QUEUES_ENABLE 
xQueueHandle  xQueue;
xQueueHandle  stQueue;

typedef struct 
{
    /* data */
    uint8_t value;
    bool status;
    uint8_t data[32];
}demo_struct_queues_t;

demo_struct_queues_t demo_struct = 
{
    .value = 1,
    .status = true,
    .data = "demo_struct_queues",
};

int arr[5] = {0,1,2,3,4};


void vSenderTask2( void *pvParameters )
{
    demo_struct_queues_t __demo_task2;
    __demo_task2.status = false;
    for(;;)
    {
        xQueueSend( stQueue, &__demo_task2, portMAX_DELAY );
        __demo_task2.value += 2;
        printf("---- %s-%d: sender 2 \r\n", __func__, xTaskGetTickCount());
        vTaskDelay(1000);
    }
    
}



void vSenderTask( void *pvParameters )
{

    int *lValueToSend;
    int len = 0;
    xTaskCreate(vSenderTask2, "vSenderTask2", 2048, NULL, 3, NULL);
    lValueToSend = arr;
    for( ;; )
    {
#if 0
        xQueueSend( xQueue, &arr[len], portMAX_DELAY );
        printf( "Sent = %d\r\n", *lValueToSend);
        
        if(len == 4)
        {
           // lValueToSend = &arr[0];
            len = 0;
        }
        else
        {
            len ++;
            /* code */
        }
        taskYIELD();
        // vTaskDelay(1000);
#else
    xQueueSend( stQueue, &demo_struct, portMAX_DELAY );
    demo_struct.value ++;
    printf("---- %s-%d: sender 1 \r\n", __func__, xTaskGetTickCount());
    vTaskDelay(1000);
#endif
    }
}

void vReceiverTask( void *pvParameters )
{

    // xTaskCreate(blink_task, "blink_task", 1024, NULL, 5, NULL);
    int lReceivedValue;
    demo_struct_queues_t __demo_struct = {0};
    for( ;; )
    {
#if 0
        if (xQueueReceive( xQueue, &lReceivedValue, portMAX_DELAY ) == pdPASS)
        {
            printf( "Received = %d\r\n", lReceivedValue);
        }
        taskYIELD();
        //vTaskDelay(1000);
#else
    if (xQueueReceive( stQueue, &__demo_struct, portMAX_DELAY ) == pdPASS)
    {
        if(__demo_struct.status == false)
            printf( "---- %s-%d:Received from task 2 = %d\r\n", __func__, xTaskGetTickCount(), __demo_struct.value);
        else
        {
            printf( "---- %s-%d:Received from task 1 = %d\r\n", __func__, xTaskGetTickCount(), __demo_struct.value);
        }
        
    }
    taskYIELD();
#endif
    }
}

#endif

#define mainAUTO_RELOAD_TIMER_PERIOD    pdMS_TO_TICKS( 100 )
uint8_t blink = 0;
void pfnTimer_ms_callback(TimerHandle_t timer_handle)
{
    TickType_t xTimeNow;
    /* Obtain the current tick count. */
    xTimeNow = xTaskGetTickCount();
    /* Output a string to show the time at which the callback was executed. */
    printf("Auto-reload timer callback executing, %d \r\n", xTaskGetTickCount());
    gpio_set_level(BLINK_GPIO, blink);
    blink = !blink;
}




#ifdef USE_SEMEPHORE 

//de thay su khac biet, comment ko su dung semephore
SemaphoreHandle_t xBinarySemaphore;
SemaphoreHandle_t xCountingSemaphore;
void LedOnTask(void *pvParameters)
{
  while(1)
  {
#if 1
    xSemaphoreTake(xBinarySemaphore,portMAX_DELAY);
    printf("Inside LedOnTask, %d\r\n", xTaskGetTickCount());
    gpio_set_level(BLINK_GPIO,0);
    vTaskDelay(500);
    xSemaphoreGive(xBinarySemaphore);
#else
   // xSemaphoreTake(xCountingSemaphore,portMAX_DELAY);
    printf("Inside LedOnTask, %d\r\n", xTaskGetTickCount());
    gpio_set_level(BLINK_GPIO,0);
   // xSemaphoreGive(xCountingSemaphore);
    vTaskDelay(500);
#endif
  }
}
void LedoffTask(void *pvParameters)
{
  while(1)
  {
#if 1
    xSemaphoreTake(xBinarySemaphore,portMAX_DELAY);
    printf("Inside LedoffTask, %d\r\n", xTaskGetTickCount());
    gpio_set_level(BLINK_GPIO,1);
    vTaskDelay(500);
    xSemaphoreGive(xBinarySemaphore);
#else
    //xSemaphoreTake(xCountingSemaphore,portMAX_DELAY);
    printf("Inside LedoffTask, %d\r\n", xTaskGetTickCount());
    gpio_set_level(BLINK_GPIO,1);
   // xSemaphoreGive(xCountingSemaphore);
    vTaskDelay(500);
#endif
  }
}

#endif

#ifdef USE_MUTEX

//de thay su khac biet, comment ko su dung semephore
SemaphoreHandle_t  xMutex;
int a = 0;
void LedOnTask(void *pvParameters)
{
  while(1)
  {
#if 1
    // xSemaphoreTake(xMutex, portMAX_DELAY);
    gpio_set_level(BLINK_GPIO,0);
    
    printf("Inside LedOnTask, a = %d\r\n", a);
    a = a+ 1;
    printf("Inside LedOnTask comback, a = %d\r\n", a);
    // xSemaphoreGive(xMutex); // release mutex
    vTaskDelay(100);
#else
   // xSemaphoreTake(xCountingSemaphore,portMAX_DELAY);
    printf("Inside LedOnTask, %d\r\n", xTaskGetTickCount());
    gpio_set_level(BLINK_GPIO,0);
   // xSemaphoreGive(xCountingSemaphore);
    vTaskDelay(500);
#endif
  }
}
void LedoffTask(void *pvParameters)
{
  while(1)
  {
#if 1
    // take mutex
    // xSemaphoreTake(xMutex, portMAX_DELAY);
    printf("---- Inside LedoffTask, a2 =  %d\r\n", a);
    a = a + 2;
    gpio_set_level(BLINK_GPIO,1);
    printf("---- Inside LedoffTask comback, a2 =  %d\r\n", a);
    // xSemaphoreGive(xMutex); // release mutex
    vTaskDelay(100);
#else
    //xSemaphoreTake(xCountingSemaphore,portMAX_DELAY);
    printf("Inside LedoffTask, %d\r\n", xTaskGetTickCount());
    gpio_set_level(BLINK_GPIO,1);
   // xSemaphoreGive(xCountingSemaphore);
    vTaskDelay(500);
#endif
  }
}

#endif


void app_main()
{
    gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

    
#ifdef EX_QUEUES_ENABLE

    xQueue = xQueueCreate( 5, sizeof( int ) );

    stQueue = xQueueCreate( 1, sizeof( demo_struct_queues_t ) );

    TimerHandle_t xAutoReloadTimer;
    xAutoReloadTimer = xTimerCreate("xAutoReloadTimer", mainAUTO_RELOAD_TIMER_PERIOD, pdTRUE, 0, pfnTimer_ms_callback );
    xTimerStart( xAutoReloadTimer, 0 );
    // xTaskCreate(blink_task, "blink_task", 1024, NULL, 5, NULL);
    xTaskCreate(vSenderTask, "vSenderTask", 2048, NULL, 4, NULL);
    xTaskCreate(vReceiverTask, "vReceiverTask", 2048, NULL, 5, NULL);
#endif

#ifdef USE_SEMEPHORE
    xCountingSemaphore = xSemaphoreCreateCounting(1,1);
   
    xBinarySemaphore = xSemaphoreCreateBinary();
    xTaskCreate(LedoffTask, "LedoffTask", 2048, NULL, 5, NULL);
    xTaskCreate(LedOnTask, "LedOnTask", 2048, NULL, 5, NULL);
    
    xSemaphoreGive(xBinarySemaphore);
    xSemaphoreGive(xCountingSemaphore);
#endif

#ifdef USE_MUTEX
    
    xMutex = xSemaphoreCreateMutex();

    xTaskCreate(LedoffTask, "LedoffTask", 2048, NULL, 3, NULL);
    xTaskCreate(LedOnTask, "LedOnTask", 2048, NULL, 4, NULL);
#endif


}