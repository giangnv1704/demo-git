1. trong qua trình code, sẽ có lúc muốn check sự thay đổi giữa các file, các lần commit
==> sử dụng lệnh git diff [--opt]
    --opt: --file1 --file2

2. để phục hồi 1 commit hoặc 1 file trong 1 commit nào đó
    git checkout [hashcode] --[file_recover]

3. trong trường hợp mà không có nhiều thay đổi, muốn merger/muốn dùng lại 1 commit thì dùng command
    git commit --amend -m "noi_dung_commit"

4. lỡ commit nhầm, xóa commit, tìm hiểu :
    git reset --[soft/hard] HEAD~1

5. phục hồi file khi đã commit vào staged.
    git restore --[file_name]/.
6. merge branch
    muốn merge branch dev vào branch master thì đầu tiên là checkout về nhanh master
    git checkout/switch master
    git merge dev
    tại đây có thể xảy ra conflict
    để hủy bỏ merge ==> git merge --abort
7. xoa branch trên local
    git branch -D <ten_branch>
8. xoa branch trên remote
    git push --delete origin <ten_nhanh>

9. lamf việc với submodule https://phannhatchanh.com/huong-dan-su-dung-git-submodule/ 
    git submodule add <repo>	Thêm một mô-đun con trong một kho lưu trữ.
    git submodule update	Cập nhật các mô-đun con hiện có trong kho lưu trữ 
                            (thêm --remote để kéo từ một vị trí từ xa).
    git submodule init	Khởi tạo tệp mô-đun con cục bộ 
                        (sử dụng nếu repo không được sao chép với --recurse-submodules.


    9.1. cấu hình để tự động cập nhật các module con
        git config --global submodule.recurse true

    9.2 add submodule ( add )
        git submodule add <repo> <ten_path_trong_project>
        git submodule add git@github.com:giangnv1993/demo_submodule.git src

    9.3. có thể chỉnh sửa code trong submodule và push lên remote repo của nó
        git push <remote> <branch> : hạn chế
        cách tốt nhất là chỉnh sửa tại repo, sau đó từ project pull về
    9.4 remove repo submodule nếu add nhầm

    9.5 pull từ 1 may tính khác
        git clone --recursive <repo>
    9.6 tao 1 submodule tu project
        add module do len 1 repo
        add repo do vao project nhu la 1 sub module
10. lam viec voi remote
    10.1 add new remote to project
        git remote add <name_remote> <link>
        git remote add GHub git@github.com:giangnv1993/Esp32-CPAP.git
        git pull GHub HEAD (pull ve neu github la remote vua add them vao repo)

